package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.controller.flower.Flower;
import com.epam.rd.java.basic.task8.controller.flower.GrowingTips;
import com.epam.rd.java.basic.task8.controller.flower.VisualParameters;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class DOMController implements Controller {
    private static final Comparator<Flower> AVERAGE_LENGTH_COMPARATOR = (o1, o2) ->
            o2.getVisualParameters().getAveLenFlower() - o1.getVisualParameters().getAveLenFlower();
    private final String XML_FILE_NAME;

    public DOMController(String xmlFileName) {
        this.XML_FILE_NAME = xmlFileName;
    }

    public List<Flower> getData() {
        File xmlFile = new File(XML_FILE_NAME);
        List<Flower> flowers = new ArrayList<>();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;

        try {
            builder = factory.newDocumentBuilder();
            Document document = builder.parse(xmlFile);
            document.getDocumentElement().normalize();

            NodeList nodeList = document.getElementsByTagName("flower");

            for (int i = 0; i < nodeList.getLength(); i++) {
                flowers.add(getFlower(nodeList.item(i)));
            }
        } catch (Exception exc) {
            exc.printStackTrace();
        }

        return flowers;
    }

    private Flower getFlower(Node item) {
        Flower flower = new Flower();
        VisualParameters visualParameters = new VisualParameters();
        GrowingTips growingTips = new GrowingTips();
        flower.setVisualParameters(visualParameters);
        flower.setGrowingTips(growingTips);

        if (item.getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) item;
            flower.setName(getTagValue("name", element));
            flower.setSoil(getTagValue("soil", element));
            flower.setOrigin(getTagValue("origin", element));
            visualParameters.setAveLenFlower(Integer.parseInt(getTagValue("aveLenFlower", element)));
            visualParameters.setMeasure(getTagAttributeValue("aveLenFlower", "measure", element));
            visualParameters.setLeafColour(getTagValue("leafColour", element));
            visualParameters.setStemColour(getTagValue("stemColour", element));
            growingTips.setTemperature(Integer.parseInt(getTagValue("tempreture", element)));
            growingTips.setTempretureMeasure(getTagAttributeValue("tempreture", "measure", element));
            growingTips.setLightRequiring(getTagAttributeValue("lighting", "lightRequiring", element));
            growingTips.setWatering(Integer.parseInt(getTagValue("watering", element)));
            growingTips.setWateringMeasure(getTagAttributeValue("watering", "measure", element));
            flower.setMultiplying(getTagValue("multiplying", element));
        }

        return flower;
    }

    private String getTagValue(String name, Element element) {
        NodeList nodeList = element.getElementsByTagName(name).item(0).getChildNodes();
        Node node = nodeList.item(0);
        return node.getNodeValue();
    }

    private String getTagAttributeValue(String tagName, String attributeName, Element element) {
        return element
                .getElementsByTagName(tagName).item(0)
                .getAttributes().getNamedItem(attributeName)
                .getTextContent();
    }

    /**
     * Sort data by average length in descending order.
     *
     * @param data data to sort
     * @return sorted data
     */
    public List<Flower> sortData(List<Flower> data) {
        return data.stream().sorted(AVERAGE_LENGTH_COMPARATOR).collect(Collectors.toList());
    }

    public void writeData(List<Flower> data, String xmlFileName) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;

        try {
            builder = factory.newDocumentBuilder();
            Document doc = builder.newDocument();
            Element rootElement = doc.createElement("flowers");
            rootElement.setAttribute("xmlns", "http://www.nure.ua");
            rootElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            rootElement.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd");

            doc.appendChild(rootElement);
            for (int i = 0; i < data.size(); i++) {
                rootElement.appendChild(getFlowerAttribute(doc, data, i));
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(doc);

            StreamResult file = new StreamResult(new File(xmlFileName));

            transformer.transform(source, file);

        } catch (ParserConfigurationException | TransformerException e) {
            e.printStackTrace();
        }

    }

    private Node getFlowerAttribute(Document doc, List<Flower> flowerList, int index) {
        Element flower = doc.createElement("flower");

        flower.appendChild(getLanguageElements(doc, "name", flowerList.get(index).getName()));
        flower.appendChild(getLanguageElements(doc, "soil", flowerList.get(index).getSoil()));
        flower.appendChild(getLanguageElements(doc, "origin", flowerList.get(index).getOrigin()));

        flower.appendChild(getFlowerAttributeVisualParameters(doc, flowerList, index));
        flower.appendChild(getFlowerAttributeGrowingTips(doc, flowerList, index));


        flower.appendChild(getLanguageElements(doc, "multiplying", flowerList.get(index).getMultiplying()));

        return flower;
    }

    private Node getFlowerAttributeVisualParameters(Document doc, List<Flower> flowerList, int index) {
        Element visualParameters = doc.createElement("visualParameters");

        visualParameters.appendChild(getLanguageElements(doc, "stemColour",
                flowerList.get(index).getVisualParameters().getStemColour()));
        visualParameters.appendChild(getLanguageElements(doc, "leafColour",
                flowerList.get(index).getVisualParameters().getLeafColour()));
        visualParameters.appendChild(getLanguageElementsAndParameters(doc, "aveLenFlower",
                flowerList.get(index).getVisualParameters().getMeasure(),
                String.valueOf(flowerList.get(index).getVisualParameters().getAveLenFlower())));

        return visualParameters;
    }

    private Node getFlowerAttributeGrowingTips(Document doc, List<Flower> flowerList, int index) {
        Element growingTips = doc.createElement("growingTips");


        growingTips.appendChild(getLanguageElementsAndParameters(doc, "tempreture",
                flowerList.get(index).getGrowingTips().getTempretureMeasure(),
                String.valueOf(flowerList.get(index).getGrowingTips().getTemperature())));
        growingTips.appendChild(getLanguageParameters(doc));
        growingTips.appendChild(getLanguageElementsAndParameters(doc, "watering",
                flowerList.get(index).getGrowingTips().getWateringMeasure(),
                String.valueOf(flowerList.get(index).getGrowingTips().getWatering())));

        return growingTips;
    }


    private Node getLanguageElements(Document doc, String name, String value) {
        Element node = doc.createElement(name);
        node.appendChild(doc.createTextNode(value));
        return node;
    }

    private Node getLanguageElementsAndParameters(Document doc, String name, String valueOnParameters, String value) {
        Element node = doc.createElement(name);
        node.setAttribute("measure", valueOnParameters);
        node.appendChild(doc.createTextNode(value));
        return node;
    }

    private Node getLanguageParameters(Document doc) {
        Element node = doc.createElement("lighting");
        node.setAttribute("lightRequiring", "yes");
        return node;
    }
}
