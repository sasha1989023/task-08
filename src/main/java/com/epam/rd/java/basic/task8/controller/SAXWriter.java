package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.controller.flower.Flower;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

public class SAXWriter {
    public void writeData(List<Flower> data, String xmlFileName) {
        try {
            StringWriter stringWriter = new StringWriter();

            XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newFactory();
            XMLStreamWriter xmlStreamWriter = xmlOutputFactory.createXMLStreamWriter(new FileOutputStream(xmlFileName));
            //Create xml document
            xmlStreamWriter.writeStartDocument();
            //Create root element
            xmlStreamWriter.writeStartElement("flowers");
            xmlStreamWriter.writeAttribute("xmlns", "http://www.nure.ua");
            xmlStreamWriter.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            xmlStreamWriter.writeAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd");

            for (Flower flower : data) {

                //Create flower element
                xmlStreamWriter.writeStartElement("flower");


                //Create flower parameters
                xmlStreamWriter.writeStartElement("name");
                xmlStreamWriter.writeCharacters(flower.getName());
                xmlStreamWriter.writeEndElement();

                xmlStreamWriter.writeStartElement("soil");
                xmlStreamWriter.writeCharacters(flower.getSoil());
                xmlStreamWriter.writeEndElement();

                xmlStreamWriter.writeStartElement("origin");
                xmlStreamWriter.writeCharacters(flower.getOrigin());
                xmlStreamWriter.writeEndElement();

                //Create visual parameters
                xmlStreamWriter.writeStartElement("visualParameters");
                xmlStreamWriter.writeStartElement("stemColour");
                xmlStreamWriter.writeCharacters(flower.getVisualParameters().getLeafColour());
                xmlStreamWriter.writeEndElement();

                xmlStreamWriter.writeStartElement("leafColour");
                xmlStreamWriter.writeCharacters(flower.getVisualParameters().getLeafColour());
                xmlStreamWriter.writeEndElement();

                xmlStreamWriter.writeStartElement("aveLenFlower");
                xmlStreamWriter.writeAttribute("measure", flower.getVisualParameters().getMeasure());
                xmlStreamWriter.writeCharacters(String.valueOf(flower.getVisualParameters().getAveLenFlower()));
                //Close create visual parameters
                xmlStreamWriter.writeEndElement();
                xmlStreamWriter.writeEndElement();

                //Create growing tips
                xmlStreamWriter.writeStartElement("growingTips");
                xmlStreamWriter.writeStartElement("tempreture");
                xmlStreamWriter.writeAttribute("measure", flower.getGrowingTips().getTempretureMeasure());
                xmlStreamWriter.writeCharacters(String.valueOf(flower.getGrowingTips().getTemperature()));
                xmlStreamWriter.writeEndElement();

                xmlStreamWriter.writeEmptyElement("lighting");
                xmlStreamWriter.writeAttribute("lightRequiring", flower.getGrowingTips().getLightRequiring());


                xmlStreamWriter.writeStartElement("watering");
                xmlStreamWriter.writeAttribute("measure", flower.getGrowingTips().getWateringMeasure());
                xmlStreamWriter.writeCharacters(String.valueOf(flower.getGrowingTips().getWatering()));
                //Close create growing tips
                xmlStreamWriter.writeEndElement();
                xmlStreamWriter.writeEndElement();

                //Create element multiplying
                xmlStreamWriter.writeStartElement("multiplying");
                xmlStreamWriter.writeCharacters(flower.getMultiplying());
                xmlStreamWriter.writeEndElement();

                //Close create flower parameters
                xmlStreamWriter.writeEndElement();
            }

            //End create document
            xmlStreamWriter.writeEndDocument();
            xmlStreamWriter.flush();
            xmlStreamWriter.close();

            String xmlString = stringWriter.getBuffer().toString();

            stringWriter.close();

            System.out.println(xmlString);
        } catch (XMLStreamException | IOException e) {
            e.printStackTrace();
        }

    }
}
