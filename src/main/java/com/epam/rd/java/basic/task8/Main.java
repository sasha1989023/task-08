package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.DOMController;
import com.epam.rd.java.basic.task8.controller.SAXController;
import com.epam.rd.java.basic.task8.controller.STAXController;
import com.epam.rd.java.basic.task8.controller.flower.Flower;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        if (args.length != 1) {
            return;
        }

        String xmlFileName = args[0];
        System.out.println("Input ==> " + xmlFileName);

        ////////////////////////////////////////////////////////
        // DOM
        ////////////////////////////////////////////////////////

        // get container
        DOMController domController = new DOMController(xmlFileName);
        // PLACE YOUR CODE HERE
        List<Flower> flowersDom = domController.getData();
        // sort (case 1)
        // PLACE YOUR CODE HERE
        List<Flower> sortedFlowersDom = domController.sortData(flowersDom);
        // save
        String outputXmlFile = "output.dom.xml";
        // PLACE YOUR CODE HERE
        domController.writeData(sortedFlowersDom, outputXmlFile);

        ////////////////////////////////////////////////////////
        // SAX
        ////////////////////////////////////////////////////////

        // get
        SAXController saxController = new SAXController(xmlFileName);
        // PLACE YOUR CODE HERE
        List<Flower> flowersSax = saxController.getData();
        // sort  (case 2)
        // PLACE YOUR CODE HERE
        List<Flower> sortedFlowersSax = saxController.sortData(flowersSax);
        // save
        outputXmlFile = "output.sax.xml";
        // PLACE YOUR CODE HERE
        saxController.writeData(sortedFlowersSax, outputXmlFile);

        ////////////////////////////////////////////////////////
        // StAX
        ////////////////////////////////////////////////////////

        // get
        STAXController staxController = new STAXController(xmlFileName);
        // PLACE YOUR CODE HERE
        List<Flower> flowersStax = staxController.getData();
        // sort  (case 3)
        // PLACE YOUR CODE HERE
        List<Flower> sortedFlowersStax = staxController.sortData(flowersStax);
        // save
        outputXmlFile = "output.stax.xml";
        // PLACE YOUR CODE HERE
        staxController.writeData(sortedFlowersStax, outputXmlFile);
    }

}
