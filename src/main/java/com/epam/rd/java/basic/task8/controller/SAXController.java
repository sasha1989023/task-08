package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.controller.flower.Flower;
import com.epam.rd.java.basic.task8.controller.flower.GrowingTips;
import com.epam.rd.java.basic.task8.controller.flower.VisualParameters;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler implements Controller {
    private static final SAXWriter SAX_WRITER = new SAXWriter();

    private List<Flower> flowersList = new ArrayList<>();
    private final String xmlFileName;
    private final StringBuilder currentValue = new StringBuilder();
    private Flower currentFlower = new Flower();

    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    @Override
    public void startDocument() {
        flowersList = new ArrayList<>();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        currentValue.setLength(0);

        switch (qName) {
            case "flower":
                currentFlower = new Flower();
                currentFlower.setGrowingTips(new GrowingTips());
                currentFlower.setVisualParameters(new VisualParameters());
                break;
            case "aveLenFlower":
                currentFlower.getVisualParameters().setMeasure(attributes.getValue("measure"));
                break;
            case "tempreture":
                currentFlower.getGrowingTips().setTempretureMeasure(attributes.getValue("measure"));
                break;
            case "watering":
                currentFlower.getGrowingTips().setWateringMeasure(attributes.getValue("measure"));
                break;
            case "lighting":
                currentFlower.getGrowingTips().setLightRequiring(attributes.getValue("lightRequiring"));
                break;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        switch (qName) {
            case "flower":
                flowersList.add(currentFlower);
                break;
            case "name":
                currentFlower.setName(currentValue.toString());
                break;
            case "soil":
                currentFlower.setSoil(currentValue.toString());
                break;
            case "origin":
                currentFlower.setOrigin(currentValue.toString());
                break;
            case "stemColour":
                currentFlower.getVisualParameters().setStemColour(currentValue.toString());
                break;
            case "leafColour":
                currentFlower.getVisualParameters().setLeafColour(currentValue.toString());
                break;
            case "aveLenFlower":
                currentFlower.getVisualParameters().setAveLenFlower(Integer.parseInt(currentValue.toString()));
                break;
            case "tempreture":
                currentFlower.getGrowingTips().setTemperature(Integer.parseInt(currentValue.toString()));
                break;
            case "watering":
                currentFlower.getGrowingTips().setWatering(Integer.parseInt(currentValue.toString()));
                break;
            case "multiplying":
                currentFlower.setMultiplying(currentValue.toString());
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        currentValue.append(ch, start, length);
    }

    @Override
    public List<Flower> getData() {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            SAXParser saxParser = factory.newSAXParser();
            saxParser.parse(xmlFileName, this);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
        return flowersList;
    }

    /**
     * Sorts data by name ascending, then by origin descending.
     *
     * @param data data to sort
     * @return sorted data
     */
    @Override
    public List<Flower> sortData(List<Flower> data) {
        return data.stream()
                .sorted(Comparator.comparing(Flower::getName)
                        .thenComparing((o1, o2) -> o2.getOrigin().compareTo(o1.getOrigin())))
                .collect(Collectors.toList());
    }

    @Override
    public void writeData(List<Flower> data, String xmlFileName) {
        SAX_WRITER.writeData(data, xmlFileName);
    }
}