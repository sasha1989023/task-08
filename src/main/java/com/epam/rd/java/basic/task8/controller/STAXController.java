package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.controller.flower.Flower;
import com.epam.rd.java.basic.task8.controller.flower.GrowingTips;
import com.epam.rd.java.basic.task8.controller.flower.VisualParameters;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.*;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler implements Controller {
    private static final SAXWriter SAX_WRITER = new SAXWriter();
    private static final Comparator<Flower> NAME_COMPARATOR = Comparator.comparing(Flower::getName);
    private final String XML_FILE_NAME;

    public STAXController(String xmlFileName) {
        this.XML_FILE_NAME = xmlFileName;
    }

    @Override
    public List<Flower> getData() {
        List<Flower> flowers = new ArrayList<>();

        try {
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLEventReader eventReader = factory.createXMLEventReader(new FileReader(XML_FILE_NAME));
            Flower flower = new Flower();
            GrowingTips growingTips = new GrowingTips();
            VisualParameters visualParameters = new VisualParameters();
            flower.setVisualParameters(visualParameters);
            flower.setGrowingTips(growingTips);

            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement()) {
                    StartElement startElement = event.asStartElement();

                    switch (startElement.getName().getLocalPart()) {
                        case "flower":
                            flower = new Flower();
                            visualParameters = new VisualParameters();
                            growingTips = new GrowingTips();
                            flower.setVisualParameters(visualParameters);
                            flower.setGrowingTips(growingTips);
                            break;

                        case "name":
                            event = eventReader.nextEvent();
                            if (event.isCharacters()) {
                                flower.setName(event.asCharacters().getData());
                            }
                            break;
                        case "soil":
                            event = eventReader.nextEvent();
                            if (event.isCharacters()) {
                                flower.setSoil(event.asCharacters().getData());
                            }
                            break;
                        case "origin":
                            event = eventReader.nextEvent();
                            if (event.isCharacters()) {
                                flower.setOrigin(event.asCharacters().getData());
                            }
                            break;
                        case "stemColour":
                            event = eventReader.nextEvent();
                            if (event.isCharacters()) {
                                visualParameters.setStemColour(event.asCharacters().getData());
                            }
                            break;
                        case "leafColour":
                            event = eventReader.nextEvent();
                            if (event.isCharacters()) {
                                visualParameters.setLeafColour(event.asCharacters().getData());
                            }
                            break;
                        case "aveLenFlower":
                            Attribute measure = startElement.getAttributeByName(new QName("measure"));
                            event = eventReader.nextEvent();
                            visualParameters.setMeasure(measure.getValue());
                            if (event.isCharacters()) {
                                visualParameters.setAveLenFlower(Integer.parseInt(event.asCharacters().getData()));
                            }
                            break;
                        case "tempreture":
                            Attribute tempMeasure = startElement.getAttributeByName(new QName("measure"));
                            event = eventReader.nextEvent();
                            growingTips.setTempretureMeasure(tempMeasure.getValue());
                            if (event.isCharacters()) {
                                growingTips.setTemperature(Integer.parseInt(event.asCharacters().getData()));
                            }
                            break;
                        case "watering":
                            Attribute waterMeasure = startElement.getAttributeByName(new QName("measure"));
                            event = eventReader.nextEvent();
                            growingTips.setWateringMeasure(waterMeasure.getValue());
                            if (event.isCharacters()) {
                                growingTips.setWatering(Integer.parseInt(event.asCharacters().getData()));
                            }
                            break;
                        case "lighting":
                            Attribute lightRequiring = startElement.getAttributeByName(new QName("lightRequiring"));
                            event = eventReader.nextEvent();
                            growingTips.setLightRequiring(String.valueOf(lightRequiring.getValue()));
                            break;
                        case "multiplying":
                            event = eventReader.nextEvent();
                            if (event.isCharacters()) {
                                flower.setMultiplying(event.asCharacters().getData());
                            }
                    }
                }

                if (event.isEndElement()) {
                    EndElement endElement = event.asEndElement();
                    if (endElement.getName().getLocalPart().equals("flower")) {
                        flowers.add(flower);
                    }
                }
            }
        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
        return flowers;
    }

    /**
     * Sorts given data by names of flowers
     *
     * @param data data to sort
     * @return sorted list
     */
    @Override
    public List<Flower> sortData(List<Flower> data) {
        return data.stream().sorted(NAME_COMPARATOR).collect(Collectors.toList());
    }

    @Override
    public void writeData(List<Flower> data, String xmlFileName) {
        SAX_WRITER.writeData(data, xmlFileName);
    }
}
