package com.epam.rd.java.basic.task8.controller.flower;

public class GrowingTips {
    private int temperature;
    private int watering;
    private String tempretureMeasure;
    private String wateringMeasure;
    private String lightRequiring;

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public int getWatering() {
        return watering;
    }

    public void setWatering(int watering) {
        this.watering = watering;
    }

    public String getLightRequiring() {
        return lightRequiring;
    }

    public void setLightRequiring(String lightRequiring) {
        this.lightRequiring = lightRequiring;
    }

    public String getTempretureMeasure() {
        return tempretureMeasure;
    }

    public void setTempretureMeasure(String tempretureMeasure) {
        this.tempretureMeasure = tempretureMeasure;
    }

    public String getWateringMeasure() {
        return wateringMeasure;
    }

    public void setWateringMeasure(String wateringMeasure) {
        this.wateringMeasure = wateringMeasure;
    }

    @Override
    public String toString() {
        return "GrowingTips{" +
                "temperature=" + temperature +
                ", watering=" + watering +
                ", tempretureMeasure='" + tempretureMeasure + '\'' +
                ", wateringMeasure='" + wateringMeasure + '\'' +
                ", lightRequiring='" + lightRequiring + '\'' +
                '}';
    }
}
