package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.controller.flower.Flower;

import java.util.List;

public interface Controller {
    /**
     * Parses data from XML to the list of flowers.
     */
    List<Flower> getData();

    /**
     * Sorts list of flowers.
     *
     * @param data data to sort
     * @return sorted data
     */
    List<Flower> sortData(List<Flower> data);

    /**
     * Writes list of flowers to the xml.
     *
     * @param data data to input to xml
     * @param xmlFileName name of the xml; if xml with such name exists, it will be overwritten
     */
    void writeData(List<Flower> data, String xmlFileName);
}
